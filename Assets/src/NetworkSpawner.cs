using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Unity;

public class NetworkSpawner : MonoBehaviour
{
    private bool spawned = true; //Bear with me
    public Transform spawnPoint;
    public GameObject spawnedPlayer;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            OnMouseDown();
        }
    }

    private void OnMouseDown() {
        if(spawned){
            NetworkManager.Instance.InstantiatePlayer();
            spawned = false;
        }
    }
}
