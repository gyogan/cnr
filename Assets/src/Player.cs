﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class Player : PlayerBehavior
{

    private static readonly float speed = 10f;

    // a^2 + b^2 = c^2, and a = b, so 2(a^2) = c^2
    // so a = SQRT((c^2) / 2), where c = SPEED
    //private static readonly float COMP_SPEED = Mathf.Sqrt(Mathf.Pow(SPEED, 2) / 2);
    private PlayerInput input;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // If this is the server or the other player, recieve transform update
        // TODO: Serverside validation can happen here with isServer
        if (!networkObject.IsOwner)
        {
            networkTransform();
        }
        else
        {
            clientControl();
        }
    }

    private void networkTransform()
    {
        transform.position = networkObject.position;
        transform.rotation = networkObject.rotation;
        return;
    }

    private void clientControl()
    {
        Vector3 pos = transform.position;

        if (Input.GetKey("w"))
        {
            pos.y += speed * Time.deltaTime;
        }
        if (Input.GetKey("s"))
        {
            pos.y -= speed * Time.deltaTime;
        }
        if (Input.GetKey("d"))
        {
            pos.x += speed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            pos.x -= speed * Time.deltaTime;
        }

        transform.position = pos;

        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = rot;

        networkObject.position = transform.position;
        networkObject.rotation = transform.rotation;
    }
}
